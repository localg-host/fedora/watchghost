Name:          watchghost
Summary:       Monitoring server
License:       LGPLv2+
URL:           https://watchg.host

Version:       0.3.0
Release:       1%{?dist}
Source0:       %{pypi_source}

# Those aren't upstream yet:
# https://gitlab.com/localg-host/watchghost/merge_requests/66
Source1:       sysusers.conf
Source2:       watchghost.service

BuildArch:     noarch

BuildRequires: python3-devel
BuildRequires: %{py3_dist aioftp}
BuildRequires: %{py3_dist aiohttp} > 3
BuildRequires: %{py3_dist aiohttp-jinja2}
BuildRequires: %{py3_dist asyncssh}
BuildRequires: %{py3_dist freezegun}
BuildRequires: %{py3_dist influxdb}
BuildRequires: %{py3_dist pytest}
BuildRequires: %{py3_dist pytest-flake8}
BuildRequires: %{py3_dist pytest-isort}
BuildRequires: %{py3_dist pytest-runner}
BuildRequires: %{py3_dist setuptools}
BuildRequires: %{py3_dist toml}
BuildRequires: %{py3_dist python-whois}

%if 0%{?epel}
BuildRequires: systemd
%else
BuildRequires: systemd-rpm-macros
%endif


%description
Watchghost is a simple to install and simple to configure monitoring server. It
is also a modern Python application making great use of asynchronous
architecture.

You have services? You need to monitor them? You don’t want to lose your time
on configuring a suuuper boring and complex monitoring system? And you don’t
want to install additionals services on your server? Watchghost is for you!


%prep
%autosetup -n %{name}-%{version} -p1


%build
%py3_build


%install
%py3_install

# Install the default configuration
install -d -m0755 %{buildroot}%{_sysconfdir}/%{name}
install -m0644 -p %{name}/etc/*.toml %{buildroot}%{_sysconfdir}/%{name}/

# Install the service
install -d -m0755 %{buildroot}%{_sysusersdir}
install -m0644 -p %{SOURCE1} %{buildroot}%{_sysusersdir}/%{name}.conf
install -d -m0755 %{buildroot}%{_unitdir}
install -m0644 -p %{SOURCE2} %{buildroot}%{_unitdir}/%{name}.service


%check
py.test


%post
if [ $1 -ge 1 ] && [ -x /usr/bin/systemd-sysusers ] ; then
    # Initial installation
    /usr/bin/systemd-sysusers %{name}.conf || :
fi
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
if [ $1 -ge 1 ] && [ -x /usr/bin/systemd-sysusers ] ; then
    # Package upgrade, not uninstall
    /usr/bin/systemd-sysusers %{name}.conf || :
fi
%systemd_postun_with_restart %{name}.service


%files
%doc README.rst
# https://gitlab.com/localg-host/watchghost/merge_requests/65
#%%license LICENSE
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/*.toml
%{_bindir}/%{name}*
%{python3_sitelib}/%{name}
%{python3_sitelib}/%{name}-%{version}-py*.egg-info
%{_unitdir}/%{name}.service
%{_sysusersdir}/%{name}.conf


%changelog
* Fri Nov 01 2019 Mathieu Bridon <bochecha@daitauha.fr> - 0.3.0-1
- Initial package for Fedora.
